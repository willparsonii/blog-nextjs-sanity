import React from 'react'
import { ComponentPreview, Previews } from '@react-buddy/ide-toolbox'
import { PaletteTree } from './palette'
import IndexPageHead from '../components/IndexPageHead'

const ComponentPreviews = () => {
  return (
    <Previews palette={<PaletteTree />}>
      <ComponentPreview path='/IndexPageHead'>
        <IndexPageHead />
      </ComponentPreview>
    </Previews>
  )
}

export default ComponentPreviews