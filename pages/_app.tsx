import 'tailwindcss/tailwind.css'

import { AppProps } from 'next/app'
import { lazy } from 'react'
import { DevSupport } from '@react-buddy/ide-toolbox'
import { ComponentPreviews, useInitial } from '../dev'

export interface SharedPageProps {
  draftMode: boolean
  token: string
}

const PreviewProvider = lazy(() => import('components/PreviewProvider'))

export default function App({
                              Component,
                              pageProps
                            }: AppProps<SharedPageProps>) {
  const { draftMode, token } = pageProps
  return (
    <>
      {draftMode ? (
        <PreviewProvider token={token}>
          <DevSupport ComponentPreviews={ComponentPreviews}
                      useInitialHook={useInitial}
          >
            <Component {...pageProps} />
          </DevSupport>
        </PreviewProvider>
      ) : (
        <Component {...pageProps} />
      )}
    </>
  )
}
