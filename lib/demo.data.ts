// All the demo data that used as fallbacks when there's nothing in the dataset yet

export const title = 'CLT Home Essentials'

export const description = [
  {
    _key: '9f1a629887fd',
    _type: 'block',
    children: [
      {
        _key: '4a58edd077880',
        _type: 'span',
        marks: [],
        text: 'Perfect Finds for the Perfect Home',
      },
      {
        _key: '4a58edd077881',
        _type: 'span',
        marks: ['ec5b66c9b1e0'],
        text: '',
      },
      {
        _key: '4a58edd077882',
        _type: 'span',
        marks: [],
        text: '',
      },
      {
        _key: '4a58edd077883',
        _type: 'span',
        marks: ['1f8991913ea8'],
        text: '',
      },
      {
        _key: '4a58edd077884',
        _type: 'span',
        marks: [],
        text: '',
      },
    ],
    markDefs: [
      {
        _key: 'ec5b66c9b1e0',
        _type: 'link',
        href: '',
      },
      {
        _key: '1f8991913ea8',
        _type: 'link',
        href: '',
      },
    ],
    style: 'normal',
  },
]

export const ogImageTitle = 'CLT Home Essentials for the Perfect Home Experience'
